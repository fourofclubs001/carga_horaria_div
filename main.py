import pandas as pd

df = pd.read_csv("CargaHorariaEventosDivsSECCB2023 (respuestas) - Respuestas de formulario 1.csv")

for divu in df["Apellido, Nombre(s)"].unique():

    df_divu = df[df["Apellido, Nombre(s)"] == divu]

    divu_publico = df_divu["a) Cantidad de horas frente a público "].str.replace(',', '.').astype(float)
    divu_interno = df_divu["b) Cantidad de horas de trabajo interno"].str.replace(',', '.').astype(float)

    print(divu + ": " + str(divu_publico.sum() + divu_interno.sum()))

    

"https://docs.google.com/spreadsheets/d/e/1Qh2lbWckQsm1MpncX0EmHOQM0SQn3L633HYwXx_-Bhg/pub?gid=685590282&single=true&output=csv"